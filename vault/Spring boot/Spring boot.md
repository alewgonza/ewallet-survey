### Provides:
Logging
Security
Metrics
DB Integration
Dependency Injection
Configuration
Microservices
Production Ready


### Roadmap

![[Pasted image 20230822195611.png]]

### Courses:
[Spring boot Tutorial 2023](https://youtu.be/9SGDpanrc8U?si=kIV-oeIXAlqbqeoc)
### Initializer
https://start.spring.io/

### Dependencies:
Spring Web
Spring Security
Spring Data JPA
PostgreSQL Driver

### Start Server

command: 
.\mvnw spring-boot:run


###  To implement new API:

![[Pasted image 20230816204509.png]]



### Integrate DB
Install PostgreSQL and create a database [[PostgreSQL]]
then in application.properties file:

	spring.datasource.url=jdbc:postgresql://localhost:5432/ewalletsurvey  
	spring.datasource.username=postgres  
	spring.datasource.password=admin  
	spring.jpa.hibernate.ddl-auto=create-drop  
	spring.jpa.show-sql=true  
	spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.PostgreSQLDialect  
	spring.jpa.properties.hibernate.format_sql=true

Make a course about this!!


### API Layer [[Controller]]

### Service Layer [[Service Layer]]

### Data Access Layer

#### Model
Create the entity using Lombok with notation
@Entity
@Table

we use SequenceGenerator instead of GenerationType.IDENTITY

#### JPA Repository
Implement the @Repository notation 
Extends JpaRepository<Class, ID type> example `JpaRepository<WUser, Long>

#### Config
Implement the @Configuration notation

CommandLineRunner




## Concepts
### Dependency Injection
DI is a design pattern used in software development to achieve loose coupling between components. In DI, instead of creating instances of required dependencies within a class, the dependencies are provided externally, typically through constructor parameters or setter methods. This promotes reusability, testability, and easier maintenance of code


To use Dependency injection in a Controller we use @Autowired, it will instanciate the parameter of the constructor before using it. And from the Service itself we use the notation @Component


**@Autowired:** In Spring, the `@Autowired` annotation is used for automatic dependency injection. When Spring encounters a field, constructor, or method annotated with `@Autowired`, it automatically injects the required dependency into that component.

**@Component:** The `@Component` annotation is used to mark a class as a Spring bean, allowing Spring to manage its lifecycle and automatically create instances of it. It's a general-purpose stereotype annotation that can be used for any class.

@Service:  is used to indicate that a particular class is a service component within your application. It's a specialization of the `@Component` annotation and is used to mark classes that handle the business logic of your application. Services typically contain the core functionality and operations of your application and are responsible for processing data, performing calculations, and implementing application-specific rules.

so we go from this:
public UserController() {  
    this.userService = new UserService();  
}

to this
@Autowired  
public UserController(UserService userService) {  
    this.userService = userService;  
}

and in the Service to be injected
@Service  
public class UserService {
}

## Concepts

### Spring-Bean

A **Spring Bean** is an object managed by the Spring Framework's Inversion of Control (IoC) container. In Spring, a bean is an instance of a class that fulfills certain requirements and is configured to be managed by the Spring container. Beans are the fundamental building blocks of a Spring application, and they represent the objects that form the backbone of your application's functionality.

Spring beans are created, configured, and managed by the Spring container, allowing you to focus on writing business logic without worrying about the intricacies of object creation and management. Spring beans can be easily wired together, injected with dependencies, and managed throughout their lifecycle.

Key characteristics of Spring beans:

1. **Managed by Spring:** Spring beans are instantiated, configured, and managed by the Spring container. This means you don't need to create instances of beans manually using the `new` keyword.
    
2. **IoC (Inversion of Control):** With Spring, the control over the instantiation and management of beans is inverted. Instead of your code creating instances of objects, the Spring container creates and injects them into the parts of your application that need them.
    
3. **Dependency Injection:** Spring beans often use dependency injection to fulfill their dependencies. Dependencies are injected into beans either through constructor injection, field injection, or setter injection.
    
4. **Lifecycle Management:** Spring beans go through various lifecycle stages, such as initialization and destruction. Spring provides hooks for you to perform custom actions during these stages.
    
5. **Configuration:** Beans are typically defined and configured in Spring configuration files (XML-based or Java-based) or through annotations. These configurations define how beans are created, their properties, and their relationships.
    
6. **Scopes:** Spring beans can have different scopes, such as singleton, prototype, request, session, etc., which determine how many instances of the bean are created and how long they live.
    
7. **AOP Support:** Spring provides support for Aspect-Oriented Programming (AOP), allowing you to separate cross-cutting concerns like logging, security, and transactions from the core business logic.
    

To summarize, a Spring bean is an object managed by the Spring container that follows the principles of Inversion of Control and Dependency Injection. It encapsulates functionality, promotes modularity, and simplifies the management of objects in your application.


### **Inversion of Control (IoC)** 
IOC is a design principle and pattern in software development that flips the traditional way of controlling the flow of a program. Instead of your code controlling the instantiation and management of objects, IoC delegates this responsibility to an external entity, often referred to as a container or framework.

IoC promotes the idea that components should be loosely coupled, meaning they should interact with each other through interfaces or contracts rather than being tightly integrated. This leads to more modular, maintainable, and testable code.

There are a few key concepts associated with Inversion of Control:

1. **Dependency Injection (DI):** DI is a fundamental aspect of IoC. It's the mechanism by which a component's dependencies are provided externally, usually through constructor injection, setter injection, or method injection. This enables the component to focus on its core functionality while delegating the responsibility of managing its dependencies to an external entity, often a container or framework.
    
2. **IoC Container:** An IoC container is responsible for creating, configuring, and managing the lifecycle of objects (usually referred to as beans). The container is aware of the beans' dependencies and their relationships, allowing it to inject the required dependencies when creating instances.
    
3. **Configuration:** With IoC, the configuration of components is often externalized. This means you define how objects are created and wired together in configuration files (XML, YAML, etc.) or through annotations in your code.
    
4. **Loose Coupling:** IoC promotes loose coupling between components, meaning that components interact through interfaces or abstractions rather than direct concrete references. This leads to code that's more modular, easier to maintain, and less prone to breaking when changes are made.
    
5. **Focus on Core Logic:** By delegating the responsibility of object creation and management to an external entity, your components can focus solely on their core logic, making them more focused, reusable, and testable.
    
6. **Testability:** IoC improves testability because you can easily replace real dependencies with mock or test-specific implementations during unit testing.
    

Frameworks like Spring, which utilize dependency injection and provide IoC containers, facilitate the implementation of IoC principles in your application. Spring's IoC container manages the creation and lifecycle of objects (beans) and injects dependencies, allowing you to build more modular and maintainable applications.

In summary, Inversion of Control is a design principle that advocates delegating the control over object creation, management, and wiring to an external entity, which leads to more flexible, modular, and maintainable software.