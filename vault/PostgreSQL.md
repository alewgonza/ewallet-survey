[# How to Install PostgreSQL 15 on Windows PC](https://youtu.be/0n41UTkOBb0?si=s4QolJG6IfCgDYXn)]

**Using PostgreSQL Shell (psql):**

PostgreSQL also provides an interactive shell called `psql` that allows you to execute SQL commands. Here's how you can use it:

a. Open a terminal or command prompt.

b. Enter the following command to connect to the PostgreSQL server:

`psql -U postgress

Replace `username` with your PostgreSQL username.

c. Once connected, you can create a new database using the `CREATE DATABASE` command:

`CREATE DATABASE ewalletsurvey;`

e. Grant privileges: 

`\du to view roles
 `GRANT ALL PRIVILEGES ON DATABASE "ewalletsurvey" TO postgres;}
 `\l

f. Set Language:
```sql
SET lc_messages TO 'en_US.UTF-8';
```

d. To exit the `psql` shell, you can use the command:

`\q`