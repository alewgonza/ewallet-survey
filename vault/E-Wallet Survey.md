## ChatGP
[[ChatGPT]]
## Initial Scope:

- User management
    
- Balance transfers between wallets within the same app
    
- Survey -> Provides feedback to the app regarding the markets to display.
- 
## Tools and plugins

MacOS: [[Commands#MacOS]]

Windows: [[Commands#Windows]]

IDE: [[IntelliJ]]

Obsidian: [https://obsidian.md/download](https://obsidian.md/download)

## GitLab Repository:

[https://gitlab.com/alewgonza/ewallet-survey](https://gitlab.com/alewgonza/ewallet-survey)

## Frameworks

Java: [[Java]]


Spring boot: [[Spring boot]]

## Database
[[PostgreSQL]]


## Project Structure:

src
├── main
│   ├── java
│   │   └── com
│   │       └── ewalletsurvey
│   │           └── app
│   │               ├── config            // Application configuration classes
│   │               ├── controller        // REST API controllers
│   │               ├── dto               // Data Transfer Objects
│   │               ├── exception         // Custom exception classes
│   │               ├── model             // Entity classes
│   │               ├── repository        // JPA repositories
│   │               ├── service           // Business logic services
│   │               ├── Application.java  // Main application class
│   │               └── SecurityConfig.java  // Spring Security configuration
│   └── resources
│       ├── application.properties         // Application properties
│       └── logback.xml                    // Log configuration
└── test
    └── java
         └── com
              └── ewalletsurvey
                └── app
                    ├── controller          // Controller unit tests
                    ├── service             // Service unit tests
                    └── repository          // Repository unit tests

### Plan:

Backend Development:

Create the required entities and tables in PostgreSQL database using Hibernate with JPA to represent user accounts, transactions, and wallet balances.

Secure user accounts and transactions by implementing user authentication and authorization using Spring Security. Develop APIs for user registration, login, wallet balance retrieval, fund transfers, and payment processing using Spring Boot and Spring Controller.

Ensure consistent error responses by implementing exception handling using Spring Controller Advice.

1- Set up PostgresSQL database with viewer

2- Connect the Database to the project

Investigar:

Elastic

Kafka

Redis

gRPC

PAT:

glpat-e3VhsNzHiDBU-oBxR764