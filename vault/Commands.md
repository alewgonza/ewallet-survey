### MacOS
Brew: [https://brew.sh/index_es](https://brew.sh/index_es)
Maven: [https://formulae.brew.sh/formula/maven](https://formulae.brew.sh/formula/maven)
JAVA: [https://formulae.brew.sh/formula/openjdk](https://formulae.brew.sh/formula/openjdk)
Git: [https://formulae.brew.sh/formula/git#default](https://formulae.brew.sh/formula/git#default)
PostgreSQL: [https://www.postgresql.org/download/macosx/](https://www.postgresql.org/download/macosx/)

### Windows
Chocolatey: [https://docs.chocolatey.org/en-us/choco/setup](https://docs.chocolatey.org/en-us/choco/setup)
Maven: [https://community.chocolatey.org/packages/maven](https://community.chocolatey.org/packages/maven)
JAVA: [https://community.chocolatey.org/packages/openjdk](https://community.chocolatey.org/packages/openjdk)
Git: [https://community.chocolatey.org/packages/git.install](https://community.chocolatey.org/packages/git.install)
Git Credentials: [https://community.chocolatey.org/packages/Git-Credential-Manager-for-Windows](https://community.chocolatey.org/packages/Git-Credential-Manager-for-Windows)
PostgreSQL: [https://www.postgresql.org/download/windows Version: 15.4](https://www.postgresql.org/download/macosx/)
	Port: default
	Pass: admin
