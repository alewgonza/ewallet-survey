Version:
Community

Plugins:
	LomBok
	.Ignore
	Key Promoter X
	Git Toolbox
	RainBox Bracket
	Csv editor
	Rainbow CSV
	IdeoLog
	Docker
	String Manipulation
	JPA Buddy
	SonarLint
	Database Navigator
	GitHub Copilot
