package com.ewalletsurvey.app;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RestController;

import java.sql.*;
import java.util.Arrays;
import java.util.logging.Logger;

@SpringBootApplication
@RestController
public class Application {

    private static final Logger LOGGER = Logger.getLogger(Application.class.getName());

    public static void main(String[] args) {
        Connection connection = null;
        Statement statement = null;
        try {
            LOGGER.info("Creating database if not exist...");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/", "postgres", "admin");
            statement = connection.createStatement();
            statement.executeQuery("SELECT count(*) FROM pg_database WHERE datname = 'ewalletsurvey'");
            ResultSet resultSet = statement.getResultSet();
            resultSet.next();
            int count = resultSet.getInt(1);

            if (count <= 0) {
                statement.executeUpdate("CREATE DATABASE ewalletsurvey");
                LOGGER.info("Database created.");
            } else {
                LOGGER.info("Database already exist.");
            }
        } catch (SQLException e) {
            LOGGER.info(e.toString());
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    LOGGER.info("Closed Statement.");
                }
                if (connection != null) {
                    LOGGER.info("Closed Connection.");
                    connection.close();
                }
            } catch (SQLException e) {
                LOGGER.info(e.toString());
            }
        }
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunnerBeanInspector(ApplicationContext ctx) {
        return args -> {

            LOGGER.info("Let's inspect the beans provided by Spring Boot:");
            String[] beanNames = ctx.getBeanDefinitionNames();
            Arrays.sort(beanNames);
            for (String beanName : beanNames) {
                LOGGER.info(beanName);
            }
        };
    }

}

/**
 * Joni
 * 1- Levantar postgresSQL local
 * 2- Al correr el comando mvnw spring-boot:run validar si existe la db y sino la deployo
 *
 * Secure user accounts with springboot transactions by implementing user authentication and authorization using Spring Security.
 * Agregar user por defecto admin
 *
 *
 * Facu
 * 1- Definir relaciones de Wusers y roles
 * 2- Definir roles y crearlos por defecto
 * 3- Configurar Relations
 */