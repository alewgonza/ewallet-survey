package com.ewalletsurvey.app.config.user;

import com.ewalletsurvey.app.model.user.WUser;
import com.ewalletsurvey.app.repository.user.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class UserConfig {

    @Bean
    CommandLineRunner commandLineRunner(UserRepository userRepository) {
        return args -> {
            WUser alex = new WUser(1L,
                    "alex",
                    "pass",
                    true);

            WUser dummy = new WUser(2L,
                    "dummy",
                    "pass",
                    true);

            userRepository.saveAll(
                    List.of(alex, dummy)
            );
        };
    }
}
