package com.ewalletsurvey.app.model.user;

import lombok.Data;
import lombok.NoArgsConstructor;
import jakarta.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    // Define the relationship with users
//    @ManyToMany(mappedBy = "roles")
//    private List<User> users;

}
