package com.ewalletsurvey.app.service.user;

import com.ewalletsurvey.app.model.user.WUser;
import com.ewalletsurvey.app.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<WUser> getUsers() {
        // acá va la logica de negocio, por ejemplo devolver usuarios activos
        return userRepository.findAll();
    }
}
