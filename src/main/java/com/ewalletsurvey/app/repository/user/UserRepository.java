package com.ewalletsurvey.app.repository.user;

import com.ewalletsurvey.app.model.user.WUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<WUser, Long> {
}
